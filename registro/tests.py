from django.test import TestCase
from django.test import Client
from .models import Persona
# Create your tests here.


class PersonaTestCase(TestCase):
    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()

    def searchPersona(self):
        persona = Persona.objects.filter(username="jose")
        self.assertEqual(Persona[0].username, "jose")

    def createAlumnoTest(self):
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="jose", email="chabucoc@gmail.com", first_name="christian", last_name="Diaz")
        persona.save()
        list = Persona.objects.all()
        self.assertGreaterEqual(len(list), 5)

class PersonaTestCase2(TestCase):
    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()

    def searchPersona(self):
        persona = Persona.objects.filter(username="carlo")
        self.assertEqual(Persona[0].username, "carlo")

    def createAlumnoTest(self):
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()
        persona = Persona.objects.create(
            username="carlo", email="cris.diaz@alumnos.duoc.cl", first_name="christian", last_name="Diaz")
        persona.save()
        list = Persona.objects.all()
        self.assertGreaterEqual(len(list), 5)


