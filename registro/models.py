from django.db import models

# Create your models here.

class Persona(models.Model):
    username = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    def __str__(self):
        return "username: " +self.username+ " first_name: "+self.first_name+" last_name: " +self.last_name+" email: "+self.email

    def get_absolute_url(self):
        return reverse('Persona-detail', args=[str(self.id)])
        