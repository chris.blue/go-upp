from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import  UserCreationForm
from django.template.context_processors import request
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from .models import Persona
from .forms import RegistroForm

# Create your views here.

def index(request):
    return render(
        request,
        'index.html',
        {} 
    )


def register(request):
    status=''
    if request.method=='POST':
        user=User()
        
        try: 
            user=User.objects.create_user(username=request.POST.get('username'),
            password=request.POST.get('password1'),
            email=request.POST.get('email'),
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'))
            username=request.POST.get('username')
            email=request.POST.get('email')
            first_name=request.POST.get('first_name')
            last_name=request.POST.get('last_name')
            persona = Persona(username=username,email=email,first_name=first_name,last_name=last_name) 
            
        except:
            status='Error'
            print(status)
            return render(
                    request,
                    'formulario.html',
                    {'status':status}
            )
        
        
        user.save(),    
        persona.save()
        status='ok'
        print(status)
        
    return render(
            request,
            'formulario.html',
            {'status':status}
    )


def Login(request):
    status = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password1')
        user = authenticate(request, username = username, password = password)
        if user:
            login(request, user)
            status = 'ok'
            return HttpResponseRedirect(reverse('index'))
        else:
            status = 'Error'
            messages.error(request,'Error al iniciar sesion')
        variables = {'status':status}
        # return render(request,'Login.html'variables)
        return render(request,'login.html',variables)
    return render(request,'login.html')

@login_required(login_url = 'Login' )
def logout_view(request):
    logout(request)
    return redirect('Login')




def buscar(request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse("nombre: "+persona.first_name+ " apellido: "+persona.last_name)

def editar(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request, 'editar.html', {'persona' : persona})

def editado(request,id):
    persona = Persona.objects.get(pk=id)
    username = request.POST.get('username', '')
    email = request.POST.get('email', '')
    first_name = request.POST.get('first_name', '')
    last_name = request.POST.get('last_name', '')
    persona.username = username
    persona.email = email
    persona.first_name = first_name
    persona.last_name = last_name
    persona.save()
    return HttpResponse("nombre: "+persona.first_name+ " apellido: "+persona.last_name)

def eliminar(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("Persona eliminada")


def listado(request):
    return render(
           request,
           'listado.html',
           {'elementos':Persona.objects.all()}
    )

def home(request):
    return render(
           request,
           'home.html',
           {}
    )



def resetkey(request):
    return render(
           request,
           'password_reset_form.html',
           {}
    )
      
def donekey(request):
    return render(
           request,
           'password_reset_done.html',
           {}
    )

def confirmkey(request):
    return render(
           request,
           'password_reset_confirm.html',
           {}
    )

def completekey(request):
    return render(
           request,
           'password_reset_complete.html',
           {}
    )    


