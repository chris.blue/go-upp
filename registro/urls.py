from django.urls import path
from django.contrib import admin
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',views.index,name="index"),    
   
    path('home/', views.home, name='home'),
    path('registro/',views.register, name="registro"),
    
    # Password reset links (ref: https://github.com/django/django/blob/master/django/contrib/auth/views.py)
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(template_name='registration/password_change_done.html'), 
        name='password_change_complete'),
    path('password_change/', auth_views.PasswordChangeView.as_view(template_name='registration/password_change.html'), 
        name='password_change'),
    path('password_reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_done.html'),
     name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'),
     name='password_reset_complete'),
    path('Login/',views.Login,name="Login"),
    path('listado/',views.listado,name="listado"),
    path('registro/buscar/<int:id>', views.buscar, name="buscar"),
    path('registro/editar/<int:id>', views.editar , name="editar"),
    path('registro/editado/<int:id>', views.editado , name="editado"),
    path('registro/eliminar/<int:id>', views.eliminar , name="eliminar"),

    path('logout/', views.logout_view, name='logout')
]

